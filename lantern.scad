module lantern(){
	color("blue") union() {
		//base
		cube(size = [5,5,1], center = true);
		//3d trap thing
		translate([0,0,0.5]) difference()
		{
			rotate([0,0,45]) cylinder(d1=sqrt(pow(5,2)*2), d2=0, h=2.25, $fn=4);
			translate([0,0,0.5]) linear_extrude(h=14) square([5,5], center=true);
		}

		#translate([0,0,5]) cylinder(r = 2, h = 7.5, center = true);

		//the bars
		translate([1.5,1.5,4.5]) cylinder(r = 0.35, h = 7.5, center = true);
		translate([-1.5,1.5,4.5]) cylinder(r = 0.35, h = 7.5, center = true);
		translate([-1.5,-1.5,4.5]) cylinder(r = 0.35, h = 7.5, center = true);
		translate([1.5,-1.5,4.5]) cylinder(r = 0.35, h = 7.5, center = true);

		//top cones
		translate([0,0,8]) scale([0.6,0.6,0.4]) difference() {
			sphere(5);
			translate([0,0,-5]) cube(size = [10,10,10], center = true);
		}
		translate([0,0,9.8]) scale([0.4,0.4,0.2]) difference() {
			sphere(5);
			translate([0,0,-7]) cube(size = [10,10,10], center = true);
		}
		translate([0,0,11]) scale([0.4,0.4,0.8]) difference() {
			sphere(1);
			translate([0,0,-7]) cube(size = [10,10,10], center = true);
		}
	}
}
