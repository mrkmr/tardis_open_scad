module notreallydoors() {
	color("blue") union(){
		difference(){
			cube(size = [36,3,78], center = true);
			translate([9,1,28]) cube(size = [12,1.5,15], center = true);
			translate([9,1,9.5]) cube(size = [12,1.5,15], center = true);
			translate([9,1,-8.5]) cube(size = [12,1.5,15], center = true);
			translate([9,1,-27]) cube(size = [12,1.5,15], center = true);
			*translate([9,1,4]) cube(size = [12,1.5,15], center = true);
			*translate([9,1,4]) cube(size = [12,1.5,15], center = true);
			*translate([9,1,4]) cube(size = [12,1.5,15], center = true);
		}
	}
}

module realleft() {
	color("blue") union(){
		cube(size = [36,2,78], center = true);

	}
}

module realright() {
	color("blue") union(){
		cube(size = [36,2,78], center = true);

	}
}

notreallydoors();
