module policesign(x,y,z) {
	translate([x,y,z])
		union() {
			color("blue") cube(size = [38,2,5.25] );
			rotate([90,0,0]) translate([0.5,0.35,-1.5])
				union() {
					color("black") square(size = [37,4.5]);
					color("white") rotate([0,180,0]) translate([-33,0.75,0.35]) scale([0.25,0.3,0.3])
						union() {
							text("POLICE");
							translate([60,5,0.3]) scale(0.4)
								union() {
									text("PUBLIC");
									translate([7,-12,0]) text("CALL");
								}
							translate ([88,0,0]) text("BOX");
						}
				}
		}
}
