include <post.scad>
include <policesign.scad>

module frame() {
	union(){
		translate([21,21,50]) post();
		translate([-21,21,50]) post();
		translate([21,-21,50]) post();
		translate([-21,-21,50]) post();
	}
	color("blue") union() {
		translate([0,0,89.75]) cube(size = [48,48,6.5], center = true);
		translate([0,0,92]) cube(size = [46,46,6.5], center = true);
		translate([0,0,96.5]) cube(size = [42,42,4.5], center = true);
		translate([0,0,100]) cube(size = [38,38,4.5], center = true);
		translate([0,0,102.2]) difference()
		{
			rotate([0,0,45]) cylinder(d1=sqrt(pow(38,2)*2), d2=0, h=2.25, $fn=4);
			translate([0,0,2]) linear_extrude(h=14) square([38,38], center=true);
		}
	}
	union() {
		policesign(-19,24,83);
		rotate([0,0,-90]) policesign(-19,24,87.75);
		rotate([0,0,90]) policesign(-19,24,87.75);
		rotate([0,0,180]) policesign(-19,24,87.75);
	}
}
