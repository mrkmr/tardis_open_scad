include <frame.scad>
include <base.scad>
include <lantern.scad>
include <doors.scad>

translate([0,0,-2]) frame();
translate([0,0,102.5]) lantern();
translate([0,20,39]) notreallydoors();
rotate([0,0,90]) translate([0,20,39]) *notreallydoors();
rotate([0,0,270]) translate([0,20,39]) *notreallydoors();
rotate([0,0,180]) translate([0,20,39]) *realleft();
rotate([0,0,180]) translate([0,20,39]) *realright();
base();
